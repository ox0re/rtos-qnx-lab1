#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv, char **envp)
{
    printf("child is reporting in with pid=%d "
           "by parent with pid=%d with args:\n", getpid(), getppid());

    for (int i = 0; i < argc; ++i)
        printf("\t%s\n", argv[i]);

    printf("env:\n");

    for (int i = 0; envp[i]; ++i)
        printf("\t%s\n", envp[i]);
}
