#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/neutrino.h>

#define DATA_LENGTH     1024 * 1024

typedef uint64_t timing_t;

enum status {
    IDLE = 0,
    EXPERIMENT = 1,
    EXIT = 2,
};

struct thread {
    pthread_t   t;
    uint64_t   *data;
    bool        ready;
    sem_t       block;
    enum status status;
};

struct thread_param {
    int prio;
    int pol;
};

struct dgrm {
    pid_t       t;
    timing_t    tstamp;
};

struct frame {
    pid_t   t;
    size_t  t_iter;
};

static const char policy[] = {'?', 'f', 'r', 'o', 's', '\0'};

/*
 * Prevents semaphore interruption by signal handlers
 *
 * */
static void sem_wait_noint(sem_t *sem)
{
    errno = EINTR;
    while (errno == EINTR) {
        errno = 0;
        sem_wait(sem);
    }
}

void *worker(void *arg)
{
    struct thread  *self = arg;

    sem_wait(&self->block);
    for (;;) {
        self->ready = true;
        sem_wait_noint(&self->block);
        self->ready = false;
        if (self->status == EXIT)
            goto exit;

        int i = 0;
        while (self->status == EXPERIMENT) {
            self->data[i++] = ClockCycles();

            if (i >= DATA_LENGTH)
                self->status = IDLE;
        }

        for (int it = i; it < DATA_LENGTH; ++it)
            self->data[it] = self->data[i];
    }

exit:
    pthread_exit(0);
    return 0;
}

static size_t get_earliest(const struct thread *threads, const struct frame *f,
        size_t count)
{
    size_t e = count;

    for (size_t i = 0; i < count; ++i) {
        if (f[i].t_iter < DATA_LENGTH) {
            e = i;
            break;
        }
    }

    if (e == count)
        return e;

    for (size_t i = e; i < count; ++i) {
        if (f[i].t_iter >= DATA_LENGTH)
            continue;

        uint64_t i_time = threads[i].data[f[i].t_iter];
        uint64_t e_time = threads[e].data[f[e].t_iter];

        if (i_time < e_time)
            e = i;
    }

    return e;
}

static struct dgrm *bld_dgrm_gnuplot(const struct thread *threads, size_t count,
                                     size_t *dgrm_len)
{
    int             d_size  = BUFSIZ;
    int             d_len   = 0;
    struct dgrm    *d       = malloc(d_size * sizeof(struct dgrm));
    struct frame   *f       = malloc(count * sizeof(struct frame));

    for (size_t i = 0; i < count; ++i) {
        f[i].t = threads[i].t;
        f[i].t_iter = 0;
    }

    size_t      current = get_earliest(threads, f, count);
    uint64_t    start   = threads[current].data[0];
    size_t      remain  = count;
    size_t      last    = current;

    d[d_len].t     = current;
    d[d_len].tstamp  = threads[current].data[f[current].t_iter] - start;
    d_len += 4;
    ++f[current].t_iter;

    while (remain) {
        current = get_earliest(threads, f, count);

        if (current != last) {
            d[d_len - 3].t    = last;
            d[d_len - 3].tstamp = threads[last].data[f[last].t_iter - 1] - start;
            d[d_len - 2].t    = -1;
            d[d_len - 2].tstamp = threads[last].data[f[last].t_iter - 1] - start + 1;
            d[d_len - 1].t    = -1;
            d[d_len - 1].tstamp = threads[current].data[f[current].t_iter] - start - 1;
            d[d_len].t     = current;
            d[d_len].tstamp  = threads[current].data[f[current].t_iter] - start;

            d_len += 4;
            if (d_len >= d_size)
                d = realloc(d, (d_size += BUFSIZ) * sizeof(struct dgrm));

            last = current;
        }

        ++f[current].t_iter;

        remain = 0;
        for (size_t i = 0; i < count; ++i)
            if (f[i].t_iter < DATA_LENGTH)
                ++remain;
    }

    d[d_len - 3].t    = last;
    d[d_len - 3].tstamp = threads[last].data[DATA_LENGTH - 1] - start;
    d[d_len - 2].t    = -1;
    d[d_len - 2].tstamp = threads[last].data[f[last].t_iter - 1] - start + 1;
    d_len -= 1;

    free(f);
    *dgrm_len = d_len;
    return d;
}

static void print_dgrm_gnuplot(struct dgrm *d, size_t d_len)
{
    for (size_t i = 0; i < d_len; ++i) {
        pid_t       t     = d[i].t;
        timing_t    cycles  = d[i].tstamp;

        printf("%" PRIu64 " %d\n", cycles, t);
    }
}

static bool all_ready(struct thread *threads, size_t count)
{
    for (size_t i = 0; i < count; ++i)
        if (!threads[i].ready)
            return false;
    return true;
}

static int experiment(struct thread *threads, size_t count)
{
    if (count < 2) {
        printf("Need 2 or more threads to experiment with\n");
        goto exit;
    }

    while (!all_ready(threads, count))
        usleep(10 * 1000);

    for (size_t i = 0; i < count; ++i) {
        threads[i].status = EXPERIMENT;
        sem_post(&threads[i].block);
    }

    sleep(2);

    for (size_t i = 0; i < count; ++i)
        threads[i].status = IDLE;

    while (!all_ready(threads, count))
        usleep(10 * 1000);

    size_t          dgrm_len = 0;
    struct dgrm    *dgrm;

    dgrm = bld_dgrm_gnuplot(threads, count, &dgrm_len);
    print_dgrm_gnuplot(dgrm, dgrm_len);
    free(dgrm);

exit:
    return 0;
}

static void ls(struct thread *threads, size_t count)
{
    for (size_t i = 0; i < count; ++i) {
        int p;
        struct sched_param param;
        pthread_getschedparam(threads[i].t, &p, &param);
        printf("# %d - %d%c\n", threads[i].t, param.sched_curpriority, policy[p]);
    }
}

void thread_setup(struct thread *t, struct thread_param p)
{
    struct sched_param  param;
    int                 dummy;

    pthread_getschedparam(t->t, &dummy, &param);
    if (p.pol == SCHED_SPORADIC) {
        nsec2timespec(&param.sched_ss_init_budget, 1000);
        nsec2timespec(&param.sched_ss_repl_period, 10000);
        param.sched_ss_low_priority = 5;
        param.sched_ss_max_repl = 10000;
    }
    param.sched_priority = p.prio;
    pthread_setschedparam(t->t, p.pol, &param);

    t->data = calloc(DATA_LENGTH, sizeof(*t->data));
    t->status = IDLE;

    sem_init(&t->block, 0, 1);
}

void thread_teardown(struct thread *t)
{
    t->status = EXIT;
    sem_post(&t->block);
    free(t->data);
}

static int get_policy(char c)
{
    for (size_t i = 1; i < sizeof(policy) - 1; ++i)
        if (c == policy[i])
            return i;
    return 3;
}

static struct thread_param *get_thread_params(int argc, char **argv,
                                              size_t *count)
{
    *count = argc - 1;
    struct thread_param *params = calloc(*count, sizeof(struct thread_param));

    for (int i = 1; i < argc; ++i) {
        params[i - 1].pol = get_policy(argv[i][strlen(argv[i]) - 1]);
        params[i - 1].prio = atoi(argv[i]);
    }

    return params;
}

int main(int argc, char **argv)
{
    size_t count;
    struct thread_param *p = get_thread_params(argc, argv, &count);
    struct thread       *t = calloc(count, sizeof(struct thread));

    for (size_t i = 0; i < count; ++i) {
        thread_setup(&t[i], p[i]);
        pthread_create(&t[i].t, NULL, worker, &t[i]);
    }
    free(p);

    ls(t, count);
    experiment(t, count);

    for (size_t i = 0; i < count; ++i) {
        thread_teardown(&t[i]);
        pthread_join(t[i].t, NULL);
    }
    free(t);
}
