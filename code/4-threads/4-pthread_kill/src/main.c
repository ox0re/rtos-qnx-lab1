#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/neutrino.h>

static volatile bool t_exit[2] = {false, false};

static void sigusr1_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    t_exit[0] = true;
}

static void sigusr2_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    t_exit[1] = true;
}

void *worker(void *arg)
{
    pthread_t *t_other = arg;
    struct timespec time;

    int i = 0;
    while (!t_exit[pthread_self() - 2]) {
        clock_gettime(CLOCK_REALTIME, &time);
        printf("T%d: %d\n", pthread_self(), time.tv_sec);
        sleep(1);
        if (i == 4) {
            if (pthread_self() == 2)
                pthread_kill(*t_other, SIGUSR2);
            else if (pthread_self() == 3)
                pthread_kill(*t_other, SIGUSR1);
        }
        ++i;
    }

    printf("T%d: Terminated\n", pthread_self());

    pthread_exit(0);
    return 0;
}

static void sigaction_simple(int sig, void (*handler)(int, siginfo_t *, void *))
{
    struct sigaction oact;
    struct sigaction act;
    act.sa_sigaction = handler;
    act.sa_flags = SA_SIGINFO;
    sigaction(sig, &act, &oact);
}

int main()
{
    pthread_t t1;
    pthread_t t2;

    sigaction_simple(SIGUSR1, sigusr1_handler);
    sigaction_simple(SIGUSR2, sigusr2_handler);

    pthread_create(&t1, NULL, worker, (void *)&t2);
    pthread_create(&t2, NULL, worker, (void *)&t1);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
}
