#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/neutrino.h>

static volatile bool t_exit[2] = {false, false};

void *worker(void *arg)
{
    (void) arg;
    struct timespec time;

    while (!t_exit[pthread_self() - 2]) {
        clock_gettime(CLOCK_REALTIME, &time);
        printf("T%d: %d\n", pthread_self(), time.tv_sec);
        sleep(1);
    }

    printf("T%d: Terminated\n", pthread_self());

    return 0;
}

static void sigusr1_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    t_exit[0] = true;
}

static void sigusr2_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    t_exit[1] = true;
}

static void sigaction_simple(int sig, void (*handler)(int, siginfo_t *, void *))
{
    struct sigaction oact;
    struct sigaction act;
    act.sa_sigaction = handler;
    act.sa_flags = SA_SIGINFO;
    sigaction(sig, &act, &oact);
}

int main()
{
    pthread_t t1;
    pthread_t t2;

    sigaction_simple(SIGUSR1, sigusr1_handler);
    sigaction_simple(SIGUSR2, sigusr2_handler);

    pthread_create(&t1, NULL, worker, NULL);
    pthread_create(&t2, NULL, worker, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
}
