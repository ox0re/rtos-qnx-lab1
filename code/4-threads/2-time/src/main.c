#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/neutrino.h>

void *worker2(void *arg)
{
    (void) arg;

    for (int i = 0; i < 2; ++i) {
        printf("T2: %d\n", i);
        sleep(5);
    }

    return 0;
}

void *worker1(void *arg)
{
    (void) arg;

    for (int i = 0; i < 10; ++i) {
        printf("T1: %d\n", i);
        sleep(1);
    }

    return 0;
}

int main()
{
    pthread_t t1;
    pthread_t t2;

    pthread_create(&t1, NULL, worker1, NULL);
    pthread_create(&t2, NULL, worker2, NULL);

    sleep(8);
    system("ps -af");
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
}
