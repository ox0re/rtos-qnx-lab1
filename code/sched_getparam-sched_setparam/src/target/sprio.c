#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <errno.h>

int main(int argc, char **argv)
{
    if (argc != 2)
        goto usage;

    struct sched_param param;

    if (sched_getparam(atoi(argv[1]), &param) < 0) {
        if (errno == EPERM)
            printf("permission denied\n");
        else if (errno == ESRCH)
            printf("no such process\n");
        else
            printf("unknown error\n");

        return -1;
    }

    printf("%d\n", param.sched_curpriority);

    return 0;
usage:
    printf("usage: sprio <pid>\n");
}
