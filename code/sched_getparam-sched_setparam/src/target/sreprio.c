#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <errno.h>

int main(int argc, char **argv)
{
    if (argc != 3)
        goto usage;

    struct sched_param param;
    param.sched_priority = atoi(argv[1]);

    if (sched_setparam(atoi(argv[2]), &param) < 0) {
        if (errno == EINVAL)
            printf("invalid priority\n");
        else if (errno == EPERM)
            printf("permission denied\n");
        else if (errno == ESRCH)
            printf("no such process\n");
        else
            printf("unknown error\n");

        return -1;
    }

    return 0;
usage:
    printf("usage: sreprio <priority> <pid>\n");
}
