#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <errno.h>

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    printf("For SCHED_FIFO policy min=%d, max=%d\n",
            sched_get_priority_min(SCHED_FIFO),
            sched_get_priority_max(SCHED_FIFO));
    printf("For SCHED_RR policy min=%d, max=%d\n",
            sched_get_priority_min(SCHED_RR),
            sched_get_priority_max(SCHED_RR));
    printf("For SCHED_OTHER policy min=%d, max=%d\n",
            sched_get_priority_min(SCHED_OTHER),
            sched_get_priority_max(SCHED_OTHER));
    printf("For SCHED_SPORADIC policy min=%d, max=%d\n",
            sched_get_priority_min(SCHED_SPORADIC),
            sched_get_priority_max(SCHED_SPORADIC));
}
