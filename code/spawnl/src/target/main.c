#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv, char **envp)
{
    (void) argc;
    (void) argv;
    char  *args[] = {"child", "arg", "bark", NULL};
    pid_t  pid;

    printf("pid = %d\n", getpid());

    printf("parent env:\n");
    for (int i = 0; envp[i]; ++i)
        printf("\t%s\n", envp[i]);
    puts("");

    if ((pid = spawnl(P_NOWAIT, args[0],
                      args[0], args[1], args[2], args[3])) == -1)
        perror("spawn() failed");
    else
        printf("spawned child, pid = %d\n", pid);

    while (1);
}
