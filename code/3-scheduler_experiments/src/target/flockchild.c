#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/iomsg.h>
#include <sys/neutrino.h>
#include <semaphore.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#define SIGRT_STARTEXPERIMENT   (SIGRTMIN + 0)
#define SIGRT_GOIDLE            (SIGRTMIN + 1)
#define SIGRT_KILL              (SIGRTMIN + 2)
#define SIGRT_YIELD             (SIGRTMIN + 3)
#define SIGRT_UNYIELD           (SIGRTMIN + 4)

typedef uint64_t timing_t;

struct aux {
    bool ready;
    bool yield;
};

enum status {
    IDLE = 0,
    EXPERIMENT = 1
};

static volatile enum status    status = IDLE;
static sem_t                   mutex;
static const int               shmem_len = 4 * 1024 * 16;
static timing_t               *shmem_ptr;
static struct aux             *aux;

static void unyield_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    aux->yield = false;
}

static void yield_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    aux->yield = true;
}

static void kill_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) siginfo;
    (void) context;
    munmap(shmem_ptr, shmem_len * sizeof(timing_t));
    exit(sig);
}

static void goidle_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    status = IDLE;
}

static void startexperiment_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) sig;
    (void) siginfo;
    (void) context;
    status = EXPERIMENT;
    sem_post(&mutex);
}

/*
 * Prevents semaphore interruption by signal handlers
 *
 * */
static void sem_wait_noint(sem_t *sem)
{
    errno = EINTR;
    while (errno == EINTR) {
        errno = 0;
        sem_wait(sem);
    }
}

static void sigaction_simple(int sig, void (*handler)(int, siginfo_t *, void *))
{
    struct sigaction oact;
    struct sigaction act;
    act.sa_sigaction = handler;
    act.sa_flags = SA_SIGINFO;
    sigaction(sig, &act, &oact);
}

int main()
{
    sigaction_simple(SIGINT, kill_handler);
    sigaction_simple(SIGRT_GOIDLE, goidle_handler);
    sigaction_simple(SIGRT_STARTEXPERIMENT, startexperiment_handler);
    sigaction_simple(SIGRT_YIELD, yield_handler);
    sigaction_simple(SIGRT_UNYIELD, unyield_handler);

    sem_init(&mutex, 0, 1);

    /* Shared memory init*/
    char   *shmem_name = malloc(BUFSIZ * sizeof(char));
    int     shmem_fd;

    sprintf(shmem_name, "pid%d", getpid());
    shmem_fd  = shm_open(shmem_name, O_RDWR, 0666);
    shmem_ptr = mmap(
            0,
            shmem_len * sizeof(timing_t) + sizeof(struct aux),
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            shmem_fd,
            0);
    free(shmem_name);
    aux = (struct aux *)&shmem_ptr[shmem_len];
    aux->ready = false;
    aux->yield = false;

    sem_wait(&mutex);
    for (;;) {
        printf("pid %d: waiting\n", getpid());
        aux->ready = true;
        sem_wait_noint(&mutex);
        aux->ready = false;
        printf("pid %d: working...\n", getpid());

        int i = 0;
        if (aux->yield) {
            while (status == EXPERIMENT) {
                shmem_ptr[i++] = ClockCycles();

                if (i >= shmem_len)
                    status = IDLE;

                sched_yield();
            }
        } else {
            while (status == EXPERIMENT) {
                shmem_ptr[i++] = ClockCycles();

                if (i >= shmem_len)
                    status = IDLE;
            }
        }

        for (int it = i; it < shmem_len; ++it)
            shmem_ptr[it] = shmem_ptr[i];
    }
}
