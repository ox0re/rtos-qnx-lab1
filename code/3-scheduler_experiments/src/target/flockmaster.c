#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <signal.h>
#include <spawn.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define SIGRT_STARTEXPERIMENT   (SIGRTMIN + 0)
#define SIGRT_GOIDLE            (SIGRTMIN + 1)
#define SIGRT_KILL              (SIGRTMIN + 2)
#define SIGRT_YIELD             (SIGRTMIN + 3)
#define SIGRT_UNYIELD           (SIGRTMIN + 4)

static int cmd_help(int argc, char **argv);
static int cmd_echo(int argc, char **argv);
static int cmd_exit(int argc, char **argv);
static int cmd_exp(int argc, char **argv);
static int cmd_ls(int argc, char **argv);
static int cmd_kill(int argc, char **argv);
static int cmd_setprio(int argc, char **argv);
static int cmd_setpol(int argc, char **argv);
static int cmd_spawn(int argc, char **argv);
static int cmd_yield(int argc, char **argv);
static int cmd_unyield(int argc, char **argv);

typedef uint64_t timing_t;

struct command {
    char   *name;
    char   *descr;
    int   (*run)(int argc, char **argv);
};

struct aux {
    bool ready;
    bool yield;
};

struct child {
    pid_t       pid;
    timing_t   *shmem_ptr;
    struct aux *aux;
};

struct dgrm {
    pid_t       pid;
    timing_t    tstamp;
};

struct frame {
    pid_t   pid;
    size_t  t_iter;
};

enum mode {
    MOD_UNKNOWN     = 0,
    MOD_INTERACTIVE = 1,
    MOD_SCRIPT      = 2
};

static struct command commands[] = {
    {"help",    "Get help",                             cmd_help},
    {"echo",    "Print text on screen",                 cmd_echo},
    {"exit",    "Quit the program",                     cmd_exit},
    {"exp",     "Perform experiment",                   cmd_exp},
    {"ls",      "List current running processes",       cmd_ls},
    {"kill",    "Kill specified process",               cmd_kill},
    {"setprio", "Set priority of number of processes",  cmd_setprio},
    {"setpol",  "Set scheduling policy",                cmd_setpol},
    {"spawn",   "Spawn new process",                    cmd_spawn},
    {"yield",   "Set processes to yield",               cmd_yield},
    {"unyield", "Unset yielding processes",             cmd_unyield},
    {0, 0, 0}
};

static const char       prefix_none[] = "";
static const char       prefix_comment[] = "# ";
static const char      *prefix = prefix_none;
static const size_t     shmem_len = 4 * 1024 * 16;
static const char      *status[] = {"", "yielding"};
static enum mode        mod = MOD_UNKNOWN;
static int              exit_f = 0;
static struct child    *childs = NULL;
static size_t           childs_iter = 0;
static size_t           childs_size = 0;
static char             policy[] = {'?', 'f', 'r', 'o', 's', '\0'};
static char            *script = NULL;
static size_t           script_size = 0;

static int cmd_help(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    printf("%savailable commands: \n", prefix);
    for (int i = 0; commands[i].name; ++i)
        printf("%s%s\t- %s\n", prefix, commands[i].name, commands[i].descr);

    return 0;
}

static int cmd_echo(int argc, char **argv)
{
    for (int i = 1; i < argc; ++i)
        printf("%s%s ", prefix, argv[i]);
    puts("\b");

    return 0;
}

static int cmd_exit(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    exit_f = 1;

    return 0;
}

static size_t get_earliest(const struct child *childs, const struct frame *f,
        size_t count)
{
    size_t e = count;

    for (size_t i = 0; i < count; ++i) {
        if (f[i].t_iter < shmem_len) {
            e = i;
            break;
        }
    }

    if (e == count)
        return e;

    for (size_t i = e; i < count; ++i) {
        if (f[i].t_iter >= shmem_len)
            continue;

        uint64_t i_time = childs[i].shmem_ptr[f[i].t_iter];
        uint64_t e_time = childs[e].shmem_ptr[f[e].t_iter];

        if (i_time < e_time)
            e = i;
    }

    return e;
}

static struct dgrm *bld_dgrm_gnuplot(const struct child *childs, size_t count,
                                     size_t *dgrm_len)
{
    int             d_size  = BUFSIZ;
    int             d_len   = 0;
    struct dgrm    *d       = malloc(d_size * sizeof(struct dgrm));
    struct frame   *f       = malloc(count * sizeof(struct frame));

    for (size_t i = 0; i < count; ++i) {
        f[i].pid = childs[i].pid;
        f[i].t_iter = 0;
    }

    size_t      current = get_earliest(childs, f, count);
    uint64_t    start   = childs[current].shmem_ptr[0];
    size_t      remain  = count;
    size_t      last    = current;

    d[d_len].pid     = current;
    d[d_len].tstamp  = childs[current].shmem_ptr[f[current].t_iter] - start;
    d_len += 4;
    ++f[current].t_iter;

    while (remain) {
        current = get_earliest(childs, f, count);

        if (current != last) {
            d[d_len - 3].pid    = last;
            d[d_len - 3].tstamp = childs[last].shmem_ptr[f[last].t_iter - 1] - start;
            d[d_len - 2].pid    = -1;
            d[d_len - 2].tstamp = childs[last].shmem_ptr[f[last].t_iter - 1] - start + 1;
            d[d_len - 1].pid    = -1;
            d[d_len - 1].tstamp = childs[current].shmem_ptr[f[current].t_iter] - start - 1;
            d[d_len].pid     = current;
            d[d_len].tstamp  = childs[current].shmem_ptr[f[current].t_iter] - start;

            d_len += 4;
            if (d_len >= d_size)
                d = realloc(d, (d_size += BUFSIZ) * sizeof(struct dgrm));

            last = current;
        }

        ++f[current].t_iter;

        remain = 0;
        for (size_t i = 0; i < count; ++i)
            if (f[i].t_iter < shmem_len)
                ++remain;
    }

    d[d_len - 3].pid    = last;
    d[d_len - 3].tstamp = childs[last].shmem_ptr[shmem_len - 1] - start;
    d[d_len - 2].pid    = -1;
    d[d_len - 2].tstamp = childs[last].shmem_ptr[f[last].t_iter - 1] - start + 1;
    d_len -= 1;

    free(f);
    *dgrm_len = d_len;
    return d;
}

static struct dgrm *bld_dgrm(const struct child *childs, size_t count,
                             size_t *dgrm_len)
{
    int             d_size  = BUFSIZ;
    int             d_len   = 0;
    struct dgrm    *d       = malloc(d_size * sizeof(struct dgrm));
    struct frame   *f       = malloc(count * sizeof(struct frame));

    for (size_t i = 0; i < count; ++i) {
        f[i].pid = childs[i].pid;
        f[i].t_iter = 0;
    }

    size_t  last    = count;
    size_t  remain  = count;
    while (remain) {
        size_t current = get_earliest(childs, f, count);

        if (current != last) {
            d[d_len].pid     = f[current].pid;
            d[d_len].tstamp  = childs[current].shmem_ptr[f[current].t_iter];
            if (++d_len >= d_size)
                d = realloc(d, (d_size += BUFSIZ) * sizeof(struct dgrm));
            last = current;
        }

        ++f[current].t_iter;

        remain = 0;
        for (size_t i = 0; i < count; ++i)
            if (f[i].t_iter < shmem_len)
                ++remain;
    }

    free(f);
    *dgrm_len = d_len;
    return d;
}

static void print_dgrm_gnuplot(struct dgrm *d, size_t d_len)
{
    for (size_t i = 0; i < d_len; ++i) {
        pid_t       pid     = d[i].pid;
        timing_t    cycles  = d[i].tstamp;

        printf("%" PRIu64 " %d\n", cycles, pid);
    }
}

static void print_dgrm(struct dgrm *d, size_t d_len)
{
    size_t prev_pid     = d[0].pid;
    time_t prev_cycles  = d[0].tstamp;

    for (size_t i = 1; i < d_len; ++i) {
        time_t cycles       = d[i].tstamp;
        time_t dif_cycles   = cycles - prev_cycles;

        printf("%spid %d exec for %d cycles\n", prefix, prev_pid, dif_cycles);

        prev_pid    = d[i].pid;
        prev_cycles = cycles;
    }
}

static bool all_ready(const struct child *childs, size_t count)
{
    for (size_t i = 0; i < count; ++i)
        if (!childs[i].aux->ready)
            return false;
    return true;
}

static int cmd_exp(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    if (childs_iter < 2) {
        printf("%sNeed 2 or more childs to experiment with\n", prefix);
        goto exit;
    }

    if (argc > 2)
        goto usage;

    while (!all_ready(childs, childs_iter))
        usleep(10 * 1000);

    for (size_t i = 0; i < childs_iter; ++i)
        kill(childs[i].pid, SIGRT_STARTEXPERIMENT);

    sleep(1);

    for (size_t i = 0; i < childs_iter; ++i)
        kill(childs[i].pid, SIGRT_GOIDLE);

    while (!all_ready(childs, childs_iter))
        usleep(10 * 1000);

    size_t dgrm_len = 0;
    struct dgrm *dgrm;

    if (argc == 2) {
        if (!strcmp("-ognuplot", argv[1])) {
            dgrm = bld_dgrm_gnuplot(childs, childs_iter, &dgrm_len);
            print_dgrm_gnuplot(dgrm, dgrm_len);
        } else {
            goto usage;
        }
    } else {
        dgrm = bld_dgrm(childs, childs_iter, &dgrm_len);
        print_dgrm(dgrm, dgrm_len);
    }

    free(dgrm);

exit:
    return 0;
usage:
    printf("%susage: exp [-gnuplot]\n", prefix);
    return 0;
}

static int kill_childs(int count)
{
    if (!childs)
        goto no_childs;

    for (; count > 0 && childs_iter > 0; --count) {
        /* Shrink size */
        if (childs_iter + BUFSIZ < childs_size)
            childs = realloc(childs, (childs_size -= BUFSIZ) * sizeof(struct child));

        if (kill(childs[--childs_iter].pid, SIGRT_KILL) < 0) {
            perror("kill() failed");
        } else {
            pid_t       pid         = childs[childs_iter].pid;
            timing_t   *shmem_ptr   = childs[childs_iter].shmem_ptr;
            childs[childs_iter].pid         = 0;
            childs[childs_iter].shmem_ptr   = NULL;
            childs[childs_iter].aux         = NULL;

            /* Free shared memeory */
            munmap(shmem_ptr, shmem_len * sizeof(timing_t) + sizeof(struct aux));
            char *shmem_name = malloc(BUFSIZ * sizeof(char));
            sprintf(shmem_name, "pid%d", pid);
            shm_unlink(shmem_name);
            free(shmem_name);

            printf("%skilled child, pid: %d\n", prefix, pid);
        }
    }

    if (childs_iter == 0)
        goto no_childs;

    return 0;
no_childs:
    printf("%sno childs left\n", prefix);
    return 0;
}

static int cmd_kill(int argc, char **argv)
{
    int count;

    if (argc > 2)
        goto usage;

    if (argc == 1) {
        count = 1;
    } else {
        count = atoi(argv[1]);
    }

    return kill_childs(count);
usage:
    printf("%susage: kill [<count>]\n", prefix);
    return 0;
}

static int cmd_ls(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    printf("%sThere are %d processes currently running\n", prefix, childs_iter);

    for (size_t i = 0; i < childs_iter; ++i) {
        pid_t pid = childs[i].pid;
        printf("%s%d priority: %d%c %s\n",
                prefix, pid, getprio(pid), policy[sched_getscheduler(pid)], status[childs[i].aux->yield]);
    }

    return 0;
}

static int cmd_setprio(int argc, char **argv)
{
    if (argc < 2 || argc > 3)
        goto usage;

    size_t  count;
    int     prio = atoi(argv[1]);

    if (argc == 2) {
        count = 1;
    } else {
        count = atoi(argv[2]);
    }

    for (size_t i = 0; i < count && i < childs_iter; ++i) {
        pid_t pid = childs[i].pid;

        if (setprio(pid, prio) < 0) {
            if (errno == EINVAL)
                perror("invalid priority");
            else if (errno == EPERM)
                perror("permission denied");
            else if (errno == ESRCH)
                perror("no such process");
        }
    }

    return 0;
usage:
    printf("%susage: setprio <prio> [<num of processes>]\n", prefix);
    return 0;
}

static int cmd_setpol(int argc, char **argv)
{
    char policy;

    if (argc != 2)
        goto usage;

    policy = *(argv[1]);

    switch (policy) {
        case 'f':
            policy = SCHED_FIFO;
            break;
        case 'r':
            policy = SCHED_RR;
            break;
        case 'o':
            policy = SCHED_OTHER;
            break;
        case 's':
            policy = SCHED_SPORADIC;
            break;
        default:
            printf("%sunknown policy %c\n", prefix, policy);
            goto usage;
    }

    for (size_t i = 0; i < childs_iter; ++i) {
        pid_t pid = childs[i].pid;
        struct sched_param param;
        if (policy == SCHED_SPORADIC) {
            nsec2timespec(&param.sched_ss_init_budget, 1000);
            nsec2timespec(&param.sched_ss_repl_period, 10000);
            param.sched_ss_low_priority = 5;
            param.sched_ss_max_repl = 10000;
        }
        sched_getparam(pid, &param);
        sched_setscheduler(pid, policy, &param);
    }

    return 0;
usage:
    printf("%susage: setpol <f|r|o|s>\n", prefix);
    return 0;
}

static int cmd_yield(int argc, char **argv)
{
    size_t count;

    if (argc > 2)
        goto usage;

    if (argc == 1) {
        count = 1;
    } else {
        count = atoi(argv[1]);
    }

    for (size_t i = 0; i < childs_iter && i < count; ++i) {
        kill(childs[i].pid, SIGRT_YIELD);
    }

    return 0;
usage:
    printf("%susage: yield [<count>]\n", prefix);
    return 0;
}

static int cmd_unyield(int argc, char **argv)
{
    size_t count;

    if (argc > 2)
        goto usage;

    if (argc == 1) {
        count = 1;
    } else {
        count = atoi(argv[1]);
    }

    for (size_t i = 0; i < childs_iter && i < count; ++i) {
        kill(childs[i].pid, SIGRT_UNYIELD);
    }

    return 0;
usage:
    printf("%susage: yield [<count>]\n", prefix);
    return 0;
}

static int cmd_spawn(int argc, char **argv)
{
    int count;

    if (argc > 2)
        goto usage;

    if (argc == 1) {
        count = 1;
    } else {
        count = atoi(argv[1]);
    }

    if (!childs)
        childs = calloc((childs_size += BUFSIZ), sizeof(struct child));

    for (; count > 0; --count) {
        /* Expand size*/
        if (childs_iter >= childs_size) {
            childs = realloc(childs, (childs_size += BUFSIZ) * sizeof(struct child));
            memset(&childs[childs_size-BUFSIZ], 0, BUFSIZ);
        }

        pid_t   pid;
        char   *args[] = {"flockchild", NULL};
        if ((pid = spawnv(P_NOWAITO, args[0], args)) == -1) {
            perror("spawn() failed");
        } else {
            /* Allocate shared mameory */
            char               *shmem_name = malloc(BUFSIZ * sizeof(char));
            int                 shmem_fd;
            timing_t           *shmem_ptr;

            sprintf(shmem_name, "pid%d", pid);
            shmem_fd = shm_open(shmem_name, O_CREAT | O_RDWR, 0666);
            ftruncate(
                    shmem_fd,
                    shmem_len * sizeof(timing_t) + sizeof(struct aux));
            shmem_ptr = mmap(
                    0,
                    shmem_len * sizeof(timing_t) + sizeof(struct aux),
                    PROT_READ,
                    MAP_SHARED,
                    shmem_fd,
                    0);
            free(shmem_name);

            /* Fill child entry */
            childs[childs_iter].pid = pid;
            childs[childs_iter].shmem_ptr = shmem_ptr;
            childs[childs_iter].aux = (struct aux *)&shmem_ptr[shmem_len];
            ++childs_iter;
            printf("%sspawned child, pid: %d\n", prefix, pid);

        }
    }

    return 0;
usage:
    printf("%susage: spawn [<count>]\n", prefix);
    return 0;
}

static void execute(int argc, char **argv)
{
    if (!argc)
        return;

    for (struct command *cmd = commands; cmd->name && cmd->run; ++cmd) {
        if (!strcmp(cmd->name, argv[0])) {
            cmd->run(argc, argv);
            return;
        }
    }

    printf("%s%s: command not found. Invoke help for help\n", prefix, argv[0]);
}

static int argtok(char ***argv, char *string)
{
    int     argc = 0;
    int     state = 0;
    ssize_t size = BUFSIZ;
    *argv = calloc(size, sizeof(char *));

    for (; *string; ++string) {
        if (*string < 33 || *string > 126) {
            *string = '\0';
            state = 0;
        } else if (!state) {
            if (argc >= size)
                argv = realloc(argv, (size += BUFSIZ) * sizeof(char));

            (*argv)[argc] = string;
            ++state;
            ++argc;
        }
    }

    return argc;
}

static int getchar_from_script() {
    static size_t i = 0;
    return (i < script_size + 1) ? script[i++] : EOF;
}

static void interpreter(int (*shell_getchar)(void))
{
    char    c;
    size_t  i = 0;
    size_t  size = BUFSIZ;
    char   *string = calloc(size, sizeof(char));

    for (;;) {
        c = shell_getchar();

        if (i >= size)
            string = realloc(string, (size += BUFSIZ) * sizeof(char));

        if (c == '\0' || c == '\r' || c == '\n') {
            string[i] = '\0';

            int     eargc = 0;
            char  **eargv = NULL;

            eargc = argtok(&eargv, string);
            execute(eargc, eargv);
            free(eargv);

            break;
        }

        if (c == EOF) {
            putchar('\n');
            exit_f = 1;
            break;
        }

        if (c == '\b') {
            putchar('\b');
            putchar(' ');
            putchar('\b');

            if (i > 0)
                string[--i] = ' ';
        } else {
            string[i++] = c;
        }
    }

    free(string);
}

static void shell(enum mode mod)
{
    if (mod == MOD_SCRIPT) {
        for (;;) {
            interpreter(getchar_from_script);

            if (exit_f)
                goto exit;
        }
    } else {
        for (;;) {
            putchar('>');
            putchar(' ');
            interpreter(getchar_unlocked);

            if (exit_f)
                goto exit;
        }
    }

exit:
    if (childs) {
        kill_childs(childs_iter);
        free(childs);
    }
}

static char *load_textfile(char *path)
{
    FILE   *fp;
    char   *text;
    int     text_size;

    fp = fopen(path, "r");
    if (!fp) {
        return 0;
    }

    fseek(fp, 0L, SEEK_END);
    text_size = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    text = malloc(text_size + 1);
    fread(text, text_size, 1, fp);
    text[text_size] = '\0';

    fclose(fp);
    return text;
}

static void sigint_handler(int sig, siginfo_t *siginfo, void *context)
{
    (void) siginfo;
    (void) context;
    kill_childs(childs_iter);
    exit(sig);
}

int main(int argc, char **argv)
{
    for (int c; (c = getopt(argc, argv, "is:")) != -1;) {
        switch (c) {
        case 'i':
            if (mod == MOD_UNKNOWN)
                mod = MOD_INTERACTIVE;
            else if (mod == MOD_SCRIPT)
                goto usage;

            break;
        case 's':
            if (mod == MOD_UNKNOWN)
                mod = MOD_SCRIPT;
            else if (mod == MOD_INTERACTIVE)
                goto usage;

            script = load_textfile(optarg);
            if (!script) {
                fprintf(stderr, "can't load script %s\n", optarg);
                goto exit;
            }
            script_size = strlen(script) + 1;

            prefix = prefix_comment;
            break;
        }
    }

    /* Signal handlers */
    struct sigaction oact;
    struct sigaction act;
    act.sa_sigaction = sigint_handler;
    act.sa_flags = SA_SIGINFO;
    sigaction(SIGINT, &act, &oact);

    if (mod == MOD_UNKNOWN) {
        goto usage;
    } else {
        shell(mod);
        goto exit;
    }

usage:
    printf("Usage: %s [options]\n\n", argv[0]);
    printf("Options:\n");
    printf("  -i\t\t\tInteractive shell (can't be used with -s)\n");
    printf("  -s <file>\t\tExecute script (can't be used with -i)\n");
exit:
    if (script)
        free(script);
}
