set terminal pngcairo size 1280,600 enhanced font 'Verdana,16'
set title "Other"
set output 'o.png'
set ylabel "Номер процесса"
set xlabel "Количество тактов"
set yr [-1:5]
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 1 \
    pointtype 7 pointsize 1
plot 'o.dat' index 0 with linespoints linestyle 1, \
