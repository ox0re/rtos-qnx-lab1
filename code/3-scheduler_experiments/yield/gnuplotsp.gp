set terminal pngcairo size 1280,600 enhanced font 'Verdana,16'
set title "Sporadic"
set output 'sp.png'
set ylabel "Номер процесса"
set xlabel "Количество тактов"
set yr [-1:3]
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 1 \
    pointtype 7 pointsize 1
plot 'sp.dat' title "Процессы" with linespoints linestyle 1, \
