#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <spawn.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
    if (argc < 2)
        return -1;

    pid_t   pid;
    int     status;

    status = posix_spawnp(&pid, argv[1], NULL, NULL, &argv[1], NULL);

    if (status >= 0) {
        printf("Child pid: %i\n", pid);

        if (wait(&status) != -1) {
            printf("a child terminated, pid = %d, status = %d\n", pid, status);
        } else {
            perror("waitpid() failed");
        }
    } else {
        perror("spawn() failed");
    }
}
