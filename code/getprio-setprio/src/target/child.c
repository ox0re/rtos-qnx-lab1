#include <stdio.h>
#include <unistd.h>

int main()
{
    printf("child is reporting in with pid=%d\n", getpid());
    fflush(stdout);

    while (1)
        sleep(1);
}
