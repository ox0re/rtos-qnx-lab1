#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>

int main(int argc, char **argv)
{
    if (argc != 2)
        goto usage;

    printf("%d\n", getprio(atoi(argv[1])));

    return 0;
usage:
    printf("usage: prio <pid>\n");
}
