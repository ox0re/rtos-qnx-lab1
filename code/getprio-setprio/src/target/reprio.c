#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <errno.h>

int main(int argc, char **argv)
{
    if (argc != 3)
        goto usage;

    if (setprio(atoi(argv[2]), atoi(argv[1])) < 0) {
        if (errno == EINVAL)
            printf("invalid priority\n");
        else if (errno == EPERM)
            printf("permission denied\n");
        else if (errno == ESRCH)
            printf("no such process\n");

        return -1;
    }

    return 0;
usage:
    printf("usage: reprio <priority> <pid>\n");
}
