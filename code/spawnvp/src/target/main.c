#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    char  *args[] = {"child", "arg", "bark", NULL};
    pid_t  pid;

    printf("pid = %d\n", getpid());

    if ((pid = spawnvp(P_NOWAIT, "./child", args)) == -1)
        perror("spawn() failed");
    else
        printf("spawned child, pid = %d\n", pid);

    while (1);
}
