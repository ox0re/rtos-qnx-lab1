#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    printf("child is reporting in with pid=%d "
           "by parent with pid=%d with args:\n", getpid(), getppid());

    for (int i = 0; i < argc; ++i)
        printf("\t%s\n", argv[i]);
}
