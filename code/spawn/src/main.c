#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    char                *args[] = {"child", NULL};
    int                 status;
    pid_t               pid;
    struct inheritance  inherit;

    for (unsigned int i = 0; i < 3; i++) {
        inherit.flags = 0;
        if ((pid = spawn(args[0], 0, NULL, &inherit, args, environ)) == -1)
            perror("spawn() failed");
        else
            printf("spawned child, pid = %d\n", pid);
    }

    while (1) {
        if ((pid = wait(&status)) == -1) {
            perror("wait() failed (no more child processes?)");
            exit(EXIT_FAILURE);
        }
        printf("a child terminated, pid = %d\n", pid);
    }
}
