#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <time.h>
#include <unistd.h>

#define HW_KYBOARD_IRQ 1

int interrupt_id;
struct sigevent event;

int main()
{
    time_t timer;
    struct tm *tm_info;
    char buffer[BUFSIZ];

    time(&timer);
    tm_info = localtime(&timer);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);
    printf("%s\n", buffer);

    ThreadCtl(_NTO_TCTL_IO, NULL);

    event.sigev_notify = SIGEV_INTR;
    interrupt_id = InterruptAttachEvent(HW_KYBOARD_IRQ, &event, _NTO_INTR_FLAGS_TRK_MSK);
    if (interrupt_id == -1) {
        printf("Couldn't attach event to IRQ %d\n", HW_KYBOARD_IRQ);
        return EXIT_FAILURE;
    }

    for (int i = 0; i < 2; i++) {
        InterruptWait(0, NULL);
        InterruptUnmask(HW_KYBOARD_IRQ, interrupt_id);
    }

    InterruptDetach(interrupt_id);
    return EXIT_SUCCESS;
}
