#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <time.h>
#include <unistd.h>
#include <hw/inout.h>

#define HW_KYBOARD_IRQ 1
#define KEYCODE_T 5140
#define KEYCODE_E 4626
#define KEYCODE_D 8224

int interrupt_id;
struct sigevent event;

int main()
{
    ThreadCtl(_NTO_TCTL_IO, NULL);

    event.sigev_notify = SIGEV_INTR;
    interrupt_id = InterruptAttachEvent(HW_KYBOARD_IRQ, &event, _NTO_INTR_FLAGS_TRK_MSK);
    if (interrupt_id == -1) {
        printf("Couldn't attach event to IRQ %d\n", HW_KYBOARD_IRQ);
        return EXIT_FAILURE;
    }

    InterruptMask(HW_KYBOARD_IRQ, interrupt_id);
    sleep(5);
    InterruptUnmask(HW_KYBOARD_IRQ, interrupt_id);

    InterruptDetach(interrupt_id);
    return EXIT_SUCCESS;
}
